<?php

namespace Recharge\Subscription\Block\Adminhtml;

/**
 *  Extension Configuration values provider.
 *
 * Class ConfigValues
 */
class ConfigValues extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * Store Name config path
     * Merchant Id config path
     */
    private const XML_PATH_STORE_NAME = 'recharge_general/general/store_name';
    private const XML_PATH_MERCHANT_ID = 'recharge_general/general/merchant_id';
    private const XML_PATH_RECHARGE_TOKEN = 'recharge_general/general/recharge_token';
    private const XML_PATH_TEST_MODE = 'recharge_general/general/test_mode';

  /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
    protected $scopeConfig;

   /**
    * ConfigValues constructor.
    *
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\App\Config\ScopeConfigInterface $productRepository
    * @param array $data
    */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function _getConfigValues()
    {
        $values = new \Magento\Framework\DataObject();

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $store_name =  $this->scopeConfig->getValue(self::XML_PATH_STORE_NAME, $storeScope);
        $values->setStoreName($store_name);
        $merchant_id =  $this->scopeConfig->getValue(self::XML_PATH_MERCHANT_ID, $storeScope);
        $values->setMerchantId($merchant_id);
        $recharge_token =  $this->scopeConfig->getValue(self::XML_PATH_RECHARGE_TOKEN, $storeScope);
        $values->setRechargeToken($recharge_token);
        $test_mode =  $this->scopeConfig->getValue(self::XML_PATH_TEST_MODE, $storeScope);
        $values->setTestMode($test_mode);
        return $values;
    }
}
