<?php

namespace Recharge\Subscription\Block\Adminhtml;

/**
 *  Extension Configuration values provider.
 *
 * Class CustomerPortal
 */
class CustomerPortal extends \Magento\Config\Block\System\Config\Form\Field
{

    private const XML_PATH_MERCHANT_ID = 'recharge_general/general/merchant_id';
    private const XML_PATH_RECHARGE_TOKEN = 'recharge_general/general/recharge_token';
    private const XML_PATH_TEST_MODE = 'recharge_general/general/test_mode';

  /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
    protected $scopeConfig;

   /**
    * CustomerPortal constructor.
    *
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\App\Config\ScopeConfigInterface $productRepository
    * @param array $data
    */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function _getCustomerPortal()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
       
        $merchant_id =  $this->scopeConfig->getValue(self::XML_PATH_MERCHANT_ID, $storeScope);
        $recharge_token =  $this->scopeConfig->getValue(self::XML_PATH_RECHARGE_TOKEN, $storeScope);
        $test_mode =  $this->scopeConfig->getValue(self::XML_PATH_TEST_MODE, $storeScope);

        $url = 'https://api.giantsqquid.com/v1/merchants/';
        if ($test_mode) {
            $url = 'https://dev1-api.giantsqquid.com/v1/merchants/';
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerEmail = '';
        if($customerSession->isLoggedIn()) {
            $customerEmail = $customerSession->getCustomer()->getEmail();
        }

        $request = new \Zend\Http\Request();
        $request->setMethod(\Zend\Http\Request::METHOD_GET);
        $request->setUri($url . $merchant_id . '/customer_portal/' . $customerEmail);
        $request->getHeaders()->addHeaders([
            'Authorization' => $recharge_token
        ]);

        $client = new \Zend\Http\Client();
        $response = $client->send($request);

        $customer_portal = htmlentities($response->getContent());

        if ($response->isOk()) {
            $customer_portal = substr($customer_portal, 6, strlen($customer_portal) - 12); // Remove the " at the beginning and in the end
        } else {
            $customer_portal = 0;
        }
        
        return $customer_portal;
    }
}
