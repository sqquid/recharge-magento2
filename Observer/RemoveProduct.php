<?php

namespace Recharge\Subscription\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 *  Sales Quote Remove Item Observer.
 *
 * Class RemoveProduct
 */
class RemoveProduct implements ObserverInterface
{
    private $rechargeSubscription = 0;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * RemoveProduct constructor.
     *
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Framework\Serialize\SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $quote = $quoteItem->getQuote();
        $cartAllItems = $quote->getAllVisibleItems();
        if ($cartAllItems) {
            foreach ($cartAllItems as $item) {
                if ($item->getSubscriptionPlan()=="subscription") {
                    $this->rechargeSubscription  = 1;
                }
            }
        }
        if ($this->rechargeSubscription) {
             $quote->setRechargeSubscription(1)->save();
        } else {
             $quote->setRechargeSubscription(0)->save();
        }
    }
}
