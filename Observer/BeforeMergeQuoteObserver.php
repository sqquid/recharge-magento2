<?php

namespace Recharge\Subscription\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

/**
 *  Before merge Quote Observer.
 *
 * Class BeforeMergeQuoteObserver
 */
class BeforeMergeQuoteObserver implements ObserverInterface
{
    public function execute(EventObserver $observer)
    {
        $customerQuote = $observer->getQuote();

        $guestQuote = $observer->getSource();
        $test = null;
        foreach ($customerQuote->getAllVisibleItems() as $item) {
            foreach ($guestQuote->getAllVisibleItems() as $guestItem) {
                $guestFrequency = $guestItem->getSubscriptionFrequency();
                $customerFrequency = $item->getSubscriptionFrequency();
                $item->setSubscriptionFrequency(0);
                $guestItem->setSubscriptionFrequency(0);
                if ($item->compare($guestItem)) {
                    $item->setSubscriptionFrequency($customerFrequency);
                    $guestItem->setSubscriptionFrequency($guestFrequency);
                    if ($item->getSubscriptionFrequency() != $guestItem->getSubscriptionFrequency()) {
                        $test = $guestItem;
                        $item->setQty($item->getQty() - $guestItem->getQty());
                    }
                    $item->setSubscriptionPlan($guestItem->getSubscriptionPlan());
                    $item->setSubscriptionUnit($guestItem->getSubscriptionUnit());
                } else {
                    $item->setSubscriptionFrequency($customerFrequency);
                    $guestItem->setSubscriptionFrequency($guestFrequency);
                }
            }
        }
    }
}
