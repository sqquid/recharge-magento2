<?php

namespace Recharge\Subscription\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

/**
 *  Sale Quote Add Item Observer.
 *
 * Class SalesQuoteAddItemObserver
 */
class SalesQuoteAddItemObserver implements ObserverInterface
{
    private $rechargeSubscription = 0;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * SalesQuoteAddItemObserver constructor.
     *
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
            $this->request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $quoteItem = $event->getQuoteItem();
        $subscriptionPlan = $this->request->getParam('purchase');
        if (!empty($subscriptionPlan)) {
                $quoteItem->setSubscriptionPlan($subscriptionPlan);
        } else {
            $quoteItem->setSubscriptionPlan(null);
        }
         $recurringMetadata = $this->request->getParam('recurring_metadata');
        if (!empty($recurringMetadata)) {
            $quoteItem->setSubscriptionUnit($recurringMetadata['unit']);
            $quoteItem->setSubscriptionFrequency($recurringMetadata['frequency']);
        } else {
            $quoteItem->setSubscriptionUnit(null);
            $quoteItem->setDsetSubscriptionFrequencyata(null);
        }
    }
}
