<?php

namespace Recharge\Subscription\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\App\RequestInterface;

/**
 *  Checkout Cart Product Update After Observer.
 *
 * Class UpdateProduct
 */
class UpdateProduct implements ObserverInterface
{

    private $rechargeSubscription = 0;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * UpdateProduct constructor.
     *
     * @param RequestInterface $request
     * @param SerializerInterface $serializer
     */
    public function __construct(
        RequestInterface $request,
        SerializerInterface $serializer
    ) {
        $this->request = $request;
        $this->serializer = $serializer;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getQuoteItem();

        $discount_type = $this->request->getParam('discount_type');
        $discount_amount = $this->request->getParam('discount_amount');
        if (!empty($discount_type) && !empty($discount_amount)) {           
            if ($quoteItem->getProduct()->getTypeId() == 'bundle') {
                foreach ($quoteItem->getQuote()->getAllItems() as $bundleitems) {
                    if ($bundleitems->getParentItemId() == $quoteItem->getId()) {
                        $price = $bundleitems->getProduct()->getPrice();
                        if ($discount_type == "percentage") {
                            $price = $price - $price * $discount_amount / 100;
                        }
                        $bundleitems->setCustomPrice($price);
                        $bundleitems->setOriginalCustomPrice($price); 
                        $bundleitems->getProduct()->setIsSuperMode(true);   
                    }
                }
                $quoteItem->getProduct()->setIsSuperMode(true);
            } else {
                $price = $quoteItem->getProduct()->getPrice();
                if ($discount_type == "percentage") {
                    $price = $price - $price * $discount_amount / 100;
                }
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }   
        }

        $subscriptionPlan = $this->request->getParam('purchase');
        if (!empty($subscriptionPlan)) {
            $quoteItem->setSubscriptionPlan($subscriptionPlan);
        } else {
            $quoteItem->setSubscriptionPlan(null);
        }

        $recurringMetadata = $this->request->getParam('recurring_metadata');
        if (!empty($recurringMetadata)) {
            $quoteItem->setSubscriptionUnit($recurringMetadata['unit']);
            $quoteItem->setSubscriptionFrequency($recurringMetadata['frequency']);
        } else {
            $quoteItem->setSubscriptionUnit(null);
            $quoteItem->setDsetSubscriptionFrequencyata(null);
        }

        $quote = $quoteItem->getQuote();
        $cartAllItems = $quote->getAllVisibleItems();
        if ($cartAllItems) {
            foreach ($cartAllItems as $item) {
                if ($item->getSubscriptionPlan()=="subscription") {
                    $this->rechargeSubscription  = 1;
                }
            }
        }

        if ($this->rechargeSubscription) {
             $quote->setRechargeSubscription(1)->save();
        } else {
             $quote->setRechargeSubscription(0)->save();
        }
    }
}
