<?php

namespace Recharge\Subscription\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 *  Sales Model Service Quote Submit Observer.
 *
 * Class SalesModelServiceQuoteSubmitBeforeObserver
 */
class SalesModelServiceQuoteSubmitBeforeObserver implements ObserverInterface
{

    private $quoteItems = [];
    private $quote = null;
    private $order = null;
    private $rechargeSubscription = 0;

    /**
     * @var LayoutInterface
     */
    protected $layout;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SalesModelServiceQuoteSubmitBeforeObserver constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param LayoutInterface $layout
     * @param RequestInterface $request
     * @param SerializerInterface $serializer
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        LayoutInterface $layout,
        RequestInterface $request,
        SerializerInterface $serializer
    ) {
        $this->layout = $layout;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->serializer = $serializer;
    }

    /**
     * Add order information into GA block to render on checkout success pages
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {

        $this->quote = $observer->getQuote();
        $this->order = $observer->getOrder();
        $cartAllItems = $this->quote->getAllVisibleItems();
        if ($cartAllItems) {
            foreach ($cartAllItems as $item) {
                if ($item->getSubscriptionPlan()) {
                    $this->rechargeSubscription  = 1;
                }
            }
        }
        if ($this->rechargeSubscription) {
              $this->order->setRechargeSubscription(1)->save();
        } else {
             $this->order->setRechargeSubscription(0)->save();
        }

        return $this;
    }
}
