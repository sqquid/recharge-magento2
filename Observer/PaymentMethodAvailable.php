<?php
namespace Recharge\Subscription\Observer;
use Magento\Framework\Event\ObserverInterface;
class PaymentMethodAvailable implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();

        if ($quote) {
            $hasSubscription = $quote->getRechargeSubscription();
            if($hasSubscription && !($observer->getEvent()->getMethodInstance()->getCode()=="mollie_methods_creditcard") && !($observer->getEvent()->getMethodInstance()->getCode()=="mollie_methods_ideal")){
                $checkResult = $observer->getEvent()->getResult();
                $checkResult->setData('is_available', false);
            }
        }
        
    }
}