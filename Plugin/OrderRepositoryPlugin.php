<?php

namespace Recharge\Subscription\Plugin;

use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderItemExtensionFactory;
use Magento\Sales\Api\Data\OrderExtensionInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 *  Add recharge_subscription attribute in order API.
 *
 * Class CustomOptionOrderView
 */
class OrderRepositoryPlugin
{
    /**
     * Recharge Subscription field name
     */
    private const FIELD_NAME = 'recharge_subscription';

    /**
     * @var OrderExtensionFactory
     */
    protected $extensionFactory;

    /**
     * @var OrderItemExtensionFactory
     */
    protected $orderItemExtensionFactory;

    /**
     * OrderRepositoryPlugin constructor
     *
     * @param OrderExtensionFactory $extensionFactory
     * @param OrderItemExtensionFactory $orderItemExtensionFactory
     */
    public function __construct(
        OrderExtensionFactory $extensionFactory,
        OrderItemExtensionFactory $orderItemExtensionFactory
    ) {
        $this->extensionFactory = $extensionFactory;
        $this->orderItemExtensionFactory = $orderItemExtensionFactory;
    }

    /**
     * Add "recharge_subscription" extension attribute to order data object to make it accessible in API
     *
     * @return OrderInterface
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $rechargesubscription = $order->getData(self::FIELD_NAME);
        $extensionAttributes = $order->getExtensionAttributes();
        $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
        $extensionAttributes->setRechargeSubscription($rechargesubscription);
        $order->setExtensionAttributes($extensionAttributes);
        $orderItems = $order->getAllItems();
        if (null !== $orderItems) {
            /** @var \Magento\Sales\Api\Data\OrderItemInterface $orderItem */
            foreach ($orderItems as $orderItem) {
                $subscriptionPlan =  $orderItem->getSubscriptionPlan();
                $subscriptionUnit =  $orderItem->getSubscriptionUnit();
                $subscriptionFrequency =  $orderItem->getSubscriptionFrequency();
                $extensionAttributesItem = $orderItem->getExtensionAttributes();
                /** @var \Magento\Sales\Api\Data\OrderItemExtension $orderItemExtension */
                $orderItemExtension = $extensionAttributesItem
                    ? $extensionAttributesItem
                    : $this->orderItemExtensionFactory->create();
                if ($subscriptionPlan) {
                    $orderItemExtension->setSubscriptionPlan($subscriptionPlan);
                }
                if ($subscriptionUnit) {
                    $orderItemExtension->setSubscriptionUnit($subscriptionUnit);
                }
                if ($subscriptionFrequency) {
                    $orderItemExtension->setSubscriptionFrequency($subscriptionFrequency);
                }
                $orderItem->setExtensionAttributes($orderItemExtension);
            }
        }

        return $order;
    }

    /**
     * Add "recharge_subscription" extension attribute to order data object to make it accessible in API
     *
     * @return OrderSearchResultInterface
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        $orders = $searchResult->getItems();

        foreach ($orders as $order) {
            $rechargesubscription = $order->getData(self::FIELD_NAME);
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
            $extensionAttributes->setRechargeSubscription($rechargesubscription);
            $order->setExtensionAttributes($extensionAttributes);
            $orderItems = $order->getAllItems();
            if (null !== $orderItems) {
                /** @var \Magento\Sales\Api\Data\OrderItemInterface $orderItem */
                foreach ($orderItems as $orderItem) {
                    $subscriptionPlan =  $orderItem->getSubscriptionPlan();
                    $subscriptionUnit =  $orderItem->getSubscriptionUnit();
                    $subscriptionFrequency =  $orderItem->getSubscriptionFrequency();
                    $extensionAttributesItem = $orderItem->getExtensionAttributes();
                    /** @var \Magento\Sales\Api\Data\OrderItemExtension $orderItemExtension */
                    $orderItemExtension = $extensionAttributesItem
                        ? $extensionAttributesItem
                        : $this->orderItemExtensionFactory->create();
                    if ($subscriptionPlan) {
                        $orderItemExtension->setSubscriptionPlan($subscriptionPlan);
                    }
                    if ($subscriptionUnit) {
                        $orderItemExtension->setSubscriptionUnit($subscriptionUnit);
                    }
                    if ($subscriptionFrequency) {
                        $orderItemExtension->setSubscriptionFrequency($subscriptionFrequency);
                    }
                    $orderItem->setExtensionAttributes($orderItemExtension);
                }
            }
        }

        return $searchResult;
    }
}
