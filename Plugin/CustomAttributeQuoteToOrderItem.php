<?php

namespace Recharge\Subscription\Plugin;

/**
 *  Add custom value in order custom field.
 *
 * Class CustomAttributeQuoteToOrderItem
 */
class CustomAttributeQuoteToOrderItem
{
    /**
     * @param \Magento\Quote\Model\Quote\Item\ToOrderItem $subject
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param \Closure $proceed
     * @param $additional
     *
     * @return object
     */
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);
        if ($item->getSubscriptionPlan()) {
            $orderItem->setSubscriptionPlan($item->getSubscriptionPlan());
        }
        if ($item->getSubscriptionUnit()) {
            $orderItem->setSubscriptionUnit($item->getSubscriptionUnit());
        }
        if ($item->getSubscriptionFrequency()) {
            $orderItem->setSubscriptionFrequency($item->getSubscriptionFrequency());
        }
        return $orderItem;
    }
}
