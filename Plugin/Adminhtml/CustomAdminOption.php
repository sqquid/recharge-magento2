<?php

namespace Recharge\Subscription\Plugin\Adminhtml;

use Magento\Sales\Block\Adminhtml\Items\Column\DefaultColumn as DefaultColumn;

/**
 *  custom option provider Plugin.
 *
 * Class CustomAdminOption
 */
class CustomAdminOption
{

    /**
     * @param DefaultColumn $subject
     * @param $result
     *
     * @return array
     */
    public function aftergetOrderOptions(DefaultColumn $subject, $result)
    {
        $item = $subject->getItem();
        $customoption = [];
        if ($item->getSubscriptionPlan() && $item->getSubscriptionUnit() && $item->getSubscriptionFrequency()) {
            $customoption[] = [
                'label' => "Subscription",
                'value' => $item->getSubscriptionFrequency() . ' ' . $item->getSubscriptionUnit(),
             ];
        }
        if (!empty($customoption)) {
            $result = array_merge($result, $customoption);
        }
        return $result;
    }
}
