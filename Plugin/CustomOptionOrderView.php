<?php

namespace Recharge\Subscription\Plugin;

use Magento\Sales\Block\Order\Item\Renderer\DefaultRenderer as DefaultRenderer;

/**
 *  custom option added in order view.
 *
 * Class CustomOptionOrderView
 */
class CustomOptionOrderView
{

    /**
     * @param DefaultRenderer $subject
     * @param $result
     *
     * @return array
     */
    public function afterGetItemOptions(DefaultRenderer $subject, $result)
    {
        $item = $subject->getItem();
        $customoption = [];
        if ($item->getSubscriptionPlan() && $item->getSubscriptionUnit() && $item->getSubscriptionFrequency()) {
            $customoption[] = [
                'label' => "Subscription",
                'value' => $item->getSubscriptionFrequency() . ' ' . $item->getSubscriptionUnit(),
             ];
        }
        if (!empty($customoption)) {
            $result = array_merge($result, $customoption);
        }
        return $result;
    }
}
