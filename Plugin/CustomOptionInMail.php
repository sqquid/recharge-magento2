<?php

namespace Recharge\Subscription\Plugin;

use Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder as DefaultOrder;

/**
 *  custom option provider for order email.
 *
 * Class CustomOptionInMail
 */
class CustomOptionInMail
{

    /**
     * @param DefaultOrder $subject
     * @param $result
     *
     * @return array
     */
    public function afterGetItemOptions(DefaultOrder $subject, $result)
    {
        $item = $subject->getItem();
        $customoption = [];
        if ($item->getSubscriptionPlan() && $item->getSubscriptionUnit() && $item->getSubscriptionFrequency()) {
            $customoption[] = [
                'label' => "Subscription",
                'value' => $item->getSubscriptionFrequency() . ' ' . $item->getSubscriptionUnit(),
             ];
        }
        if (!empty($customoption)) {
            $result = array_merge($result, $customoption);
        }
        return $result;
    }
}
