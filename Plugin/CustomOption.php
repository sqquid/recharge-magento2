<?php

namespace Recharge\Subscription\Plugin;

use Magento\Catalog\Helper\Product\Configuration as Itemrender;

/**
 *  custom option provider for cart and checkout.
 *
 * Class CustomOption
 */
class CustomOption
{

    /**
     * @param Configuration $subject
     * @param $result
     * @param $item
     *
     * @return array
     */
    public function afterGetCustomOptions(Itemrender $subject, $result, $item)
    {
        $customoption = [];
        if ($item->getSubscriptionPlan() && $item->getSubscriptionUnit() && $item->getSubscriptionFrequency()) {
            $customoption[] = [
                'label' => "Subscription",
                'value' => $item->getSubscriptionFrequency() . ' ' . $item->getSubscriptionUnit(),
             ];
        }
        if (!empty($customoption)) {
            $result = array_merge($result, $customoption);
        }
        return $result;
    }
}
