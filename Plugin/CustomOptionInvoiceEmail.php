<?php

namespace Recharge\Subscription\Plugin;

use Magento\Sales\Block\Order\Email\Items\DefaultItems as DefaultItems;

/**
 *  custom option provider for invoice email.
 *
 * Class CustomOptionInvoiceEmail
 */
class CustomOptionInvoiceEmail
{

    /**
     * @param DefaultItems $subject
     * @param $result
     *
     * @return array
     */
    public function afterGetItemOptions(DefaultItems $subject, $result)
    {
        $item = $subject->getItem()->getOrderItem();
        $customoption = [];
        if ($item->getSubscriptionPlan() && $item->getSubscriptionUnit() && $item->getSubscriptionFrequency()) {
            $customoption[] = [
                'label' => "Subscription",
                'value' => $item->getSubscriptionFrequency() . ' ' . $item->getSubscriptionUnit(),
             ];
        }
        if (!empty($customoption)) {
            $result = array_merge($result, $customoption);
        }
        return $result;
    }
}
